from pathlib import Path
from datetime import datetime

from jinja2 import Template


ROOT = Path(__file__).resolve().parent

CONTEXT = {
    "LAST_GEN": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
    "JOBS": (
        {
            "color": "blue-grey",
            "icon": "location_city",
            "title": "Netty",
            "content": "I developed a web application for real estate agents. The purpose of this request is to create a poster for each property. Thus, the real estate agent gives some photos and information about the property such as price, surface area, number of rooms, etc. Then, the application generates posters using templates.",
            "link": "https://www.netty.fr/",
        },
        {
            "color": "green",
            "icon": "smartphone",
            "title": "Natevia",
            "content": "I worked on a mobile application. It was a social network application for travellers who can share tips when they were travelling. The application is a port from an existing website, so I use the website API.",
            "link": "http://www.natevia.com/",
        },
        {
            "color": "light-blue",
            "icon": "format_color_fill",
            "title": "Bürkert",
            "content": "I developed a Python tool to test a new feature added to a controller/transmitter (MultiCELL).",
            "link": "https://www.burkert.com/",
        },
        {
            "color": "teal",
            "icon": "developer_board",
            "title": "Telemisis",
            "content": "I participated in the development of the firmware on a newly designed board. The purpose of the card is to retrieve information from various sensors, including wireless (ZigBee), and transmit it to the main device via the ModBus RTU protocol.",
            "link": "https://www.telemisis.com/",
        },
    ),
    "PROJECTS": (
        {
            "img": "static/images/jobs/logo_cuisinefit_alpha.png",
            "title": "Cuisinefit",
            "content": "A shop (and showcase) for Cuisinefit, a company which deliver a dietetic lunch directly to your gym.",
            "link": "https://www.cuisinefit.fr/",
        },
        {
            "img": "static/images/jobs/bag-innovation.jpg",
            "title": "Bag Innovation",
            "content": "School project, the goal was to take an everyday object and make innovation, try a new concept about it. For my team, we took the backpack and create a modular backpack with different compartments.",
            "link": "https://bag-innovation.romainmuller.xyz/",
        },
        {
            "img": "static/images/jobs/who-host-who.png",
            "title": "WhoHostWho",
            "content": 'A website to show technical information (IP, ASN, etc) about a website and nameservers that serve this website. The code source is <a href="https://gitlab.com/Oprax/whohostwho">available</a> to everyone !',
            "link": "http://whohostwho.oprax.fr/",
        },
    ),
    "MENUITEMS": (
        # ('<i class="fas fa-file-pdf"></i>', "/static/CV-en.pdf", "Curriculum Vitæ"),
        # ('<i class="fas fa-envelope"></i>', "mailto:contact@oprax.fr", "Email me"),
        (
            '<i class="fab fa-linkedin"></i>',
            "https://www.linkedin.com/in/romainmuller42/",
            "Linkedin",
        ),
        ('<i class="fab fa-github"></i>', "https://github.com/Oprax", "GitHub"),
        ('<i class="fab fa-gitlab"></i>', "https://gitlab.com/Oprax", "GitLab"),
        (
            '<i class="fas fa-file-pdf"></i>',
            "/static/CV.pdf",
            "Curriculum vitæ (Français)",
        ),
        (
            '<i class="far fa-file-pdf"></i>',
            "/static/CV-en.pdf",
            "Curriculum vitæ (English)",
        ),
    ),
}


def main():
    infile = ROOT / "index.html.j2"
    oufile = ROOT / "public/index.html"
    template = Template(infile.read_text(encoding="utf-8", errors="strict"))
    content = template.render(**CONTEXT)
    oufile.write_text(content, encoding="utf-8", errors="strict")


if __name__ == "__main__":
    main()
