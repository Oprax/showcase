# `builder-base` stage is used to build deps + create our virtual environment
FROM registry.oprax.fr/python:3.8-poetry as builder-base
COPY . .

# install runtime deps - uses $POETRY_VIRTUALENVS_IN_PROJECT internally
RUN poetry install --no-dev
RUN poetry run python publish.py


# `production` image used for runtime
FROM nginx:alpine as production
COPY --from=builder-base /opt/pysetup/public /usr/share/nginx/html
